/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _showblocks_showphoto_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./showblocks/showphoto.js */ \"./application/showblocks/showphoto.js\");\n\n\n\n\nObject(_showblocks_showphoto_js__WEBPACK_IMPORTED_MODULE_0__[\"showphoto\"])();\n\n// const DataArray = [\n//     {\n//         tags: ['good vibes', 'good mood'],\n//         imageUrl: 'images/1.jpg'\n//     },\n//     {\n//         tags: ['good mood'],\n//         imageUrl: 'images/2.jpg'\n//     },\n//     {\n//         tags: ['good mood'],\n//         imageUrl: 'images/3.jpg'\n//     },\n//     {\n//         tags: ['good mood'],\n//         imageUrl: 'images/4.jpg'\n//     },\n//     {\n//         tags: ['good vibes'],\n//         imageUrl: 'images/2.2.jpg'\n//     },\n//     {\n//         tags: ['good vibes'],\n//         imageUrl: 'images/3.3.jpg'\n//     },\n//     {\n//         tags: ['good vibes'],\n//         imageUrl: 'images/4.4.jpg'\n//     },\n//     {\n//         tags: ['test'],\n//         imageUrl: 'images/4.4.jpg'\n//     }\n// ];\n//\n// const HideImage = ( selector, img ) => {\n//     /*\n//         ф-я прячет картинку перед тем как добавить новую и добавляет\\убирает нужные классы для анимации\n//     */\n//     let oldImage = selector.querySelector('img');\n//     console.log( oldImage );\n//     if( oldImage !== null ){\n//         oldImage.classList.remove('animationUp');\n//         oldImage.classList.add('animationDown');\n//     }\n//     setTimeout(() => {\n//         img.classList.add(\"animationUp\");\n//         selector.innerHTML = null;\n//         selector.appendChild(img);\n//     }, 500);\n// };\n//\n// const renderImages = (array) => {\n//     /*\n//         ф-я находит нужные блоки и с задержкой 0.1+ 0.1 за итерацию добавляет картинки\n//     */\n//     let resultBlock = document.getElementById('visible');\n//     let right = document.getElementById('right');\n//     let middle_top = document.getElementById('middle_top');\n//     let middle_bottom = document.getElementById('middle_bottom');\n//     let left = document.getElementById('left');\n//\n//     console.log( \"render:\", array );\n//     let delay = 0.1;\n//     array.map( (item, key) => {\n//         let img = new Image();\n//             img.src = item.imageUrl;\n//             img.style.animationDelay = `${delay}s`;\n//\n//             delay = delay + 0.1;\n//         if( key === 0){ HideImage(left, img);}\n//         if( key === 1){ HideImage(middle_top, img);}\n//         if( key === 2 ){ HideImage(right, img);}\n//         if( key === 3 ){ HideImage(middle_bottom, img);}\n//     });\n//\n//\n// };\n//\n// const searchInput = document.getElementById('search');\n//\n// searchInput.addEventListener('input', (e) => {\n//     /*\n//         ф-я преобразовывает значение с поиска к нижнему регистру, для того что бы сравнить с каждым из тегов внутри обьекта DataArray\n//         после если количество результатов больше 3х, отправляет их в функцию рендера\n//     */\n//     let value = e.target.value.toLowerCase();\n//     let result =  DataArray.filter( item => {\n//         return item.tags.some( tag => tag.toLowerCase().indexOf(value) !== -1 );\n//     });\n//     autocomplete(value);\n//     console.log( value.length );\n//     if ( value.length > 3 ) {\n//         if( result.length >= 3 ){\n//             renderImages(result);\n//         }\n//     }\n//\n//\n//     // console.log(result);\n// });\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/showblocks/showphoto.js":
/*!*********************************************!*\
  !*** ./application/showblocks/showphoto.js ***!
  \*********************************************/
/*! exports provided: showphoto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"showphoto\", function() { return showphoto; });\n\n\n\nfunction showphoto() {\n    let input = document.getElementById('text'),\n        photoMood = [\"images/1.jpg\", \"images/2.jpg\", \"images/3.jpg\", \"images/4.jpg\"],\n        photoVibes = [\"images/1.jpg\", \"images/2.2.jpg\", \"images/3.3.jpg\", \"images/4.4.jpg\"],\n        i;\n\n\n    function RenderAnime() {\n        let InputValue = input.value,\n            addAnime = document.querySelectorAll('.addAnime');\n\n        if(InputValue === \"good mood\") {\n            for(i=0; i <= photoMood.length - 1; i++) {\n                let Image = document.createElement('img');\n                    addAnime[i].classList.remove('animationUp');\n                    Image.setAttribute('src', photoMood[i]);\n                    addAnime[i].classList.add('animationUp');\n                    addAnime[i].appendChild(Image);\n            \t}\n        }\n\n        if(InputValue === \"good vibes\"){\n            for(i=0; i<= photoVibes.length - 1; i++) {\n                let Image = document.createElement('img');\n                    Image.setAttribute('src', photoVibes[i]);\n                    addAnime[i].classList.add('animationUp');\n                    addAnime[i].appendChild(Image);\n            \t}\n        }\n\n        if( InputValue !== \"good mood\" && InputValue !== \"good vibes\" ){\n            addAnime.forEach( (item) => {\n                let oldImage = item.querySelectorAll('img');\n                    oldImage.forEach(function(element){\n                console.log(element);\n                    if( element !== null ){\n                        element.classList.add('animationDowm');\n                        item.innerHTML = null;\n                    }\n                });\n            });\n        }\n    }\n    input.addEventListener('input', RenderAnime);\n}\n\n\n//# sourceURL=webpack:///./application/showblocks/showphoto.js?");

/***/ })

/******/ });